# v0.1.2

## Changes
- much faster footprint: replace `l1c_footprint` by `s2_footprint` that reads footprint from MTD_MSIL1C.xml
making it available for L1C and LASRC.
- add_footprint now based on s2_footprint, thus removed l1c_db arg.
- add column `title` to `parse_S2_name`
- replace `check_lasrc_tile_full` by `lasrc_check_image`
