% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/functions.R
\name{add_p_title}
\alias{add_p_title}
\title{Add p_title at the top of a ggplot object or ggarrange page}
\usage{
add_p_title(p, p_title, map_tab_ratio = 7)
}
\arguments{
\item{p}{ggplot object or list of ggplot/ggarrange to which p_title is added}

\item{p_title}{ggplot object to add at the top of plot/page}

\item{map_tab_ratio}{numeric. Ratio between p height and p_title height.}
}
\value{
object of the same kind as input.
}
\description{
Add p_title at the top of a ggplot object or ggarrange page
}
