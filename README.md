# __sen2proc__

[![licence](https://img.shields.io/badge/Licence-GPL--3-blue.svg)](https://www.r-project.org/Licenses/GPL-3)

# An R Package dedicated to process Sentinel 2 LASRC corrected time series.

[Documentation](https://floriandeboissieu.gitlab.io/sen2proc)

Ee also related python package [sen2lasrc](https://gitlab.com/floriandeboissieu/sen2lasrc) to compute S2 L2A 
images corrected with LASRC.


# 1 Install

```r
remotes::install_gitlab('floriandeboissieu/sen2proc')
```

# Acknowledgements

The development of this package was supported by CIRAD and INRAE within 
project Orbital Forest Management, under the supervision of 
Guerric le Maire (guerric.le_maire@cirad.fr) 
and Jean-Baptiste Féret (jean-baptiste.feret@inrae.fr).
